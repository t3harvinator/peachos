# PeachOS

Going through Kernel Development Course on Udemy
https://digitalu.udemy.com/course/developing-a-multithreaded-kernel-from-scratch

## Real Mode
### Creating a boot loader:
BIOS looks through attached drives to find a sector ending in `55 AA`

After this, the BIOS loads byte 0x00 of that sector into `0x7C00` of memory and jumps to that program

Kernel Land: Ring 0
User Land: Ring 3

Segment Register = seg * 16 + offset

#### Compile Instructions:
```nasm -f bin ./boot.asm -o boot.bin```

#### Running Bootloader:
```qemu-system-x86_64 -hda boot.bin```



from unicorn import *
from unicorn.x86_const import *
from capstone import *

ADDRESS = 0x7C00

def hook_code(mu, address, size, user_data):
    # Read the current instruction
    instr = mu.mem_read(address, size)
    # Print the instruction
    # print("0x{:04X}: {}".format(address, instr.hex()))
    md = Cs(CS_ARCH_X86, CS_MODE_16)
    for i in md.disasm(instr, address):
        print("0x%x:\t%s\t%s" %(i.address, i.mnemonic, i.op_str))

    # Print the registers
    print("Registers:")
    print("  CS = 0x{:04X}".format(mu.reg_read(UC_X86_REG_CS)))
    print("  DS = 0x{:04X}".format(mu.reg_read(UC_X86_REG_DS)))
    print("  ES = 0x{:04X}".format(mu.reg_read(UC_X86_REG_ES)))
    print("  SS = 0x{:04X}".format(mu.reg_read(UC_X86_REG_SS)))
    print("  FS = 0x{:04X}".format(mu.reg_read(UC_X86_REG_FS)))
    print("  GS = 0x{:04X}".format(mu.reg_read(UC_X86_REG_GS)))
    print("  AX = 0x{:04X}".format(mu.reg_read(UC_X86_REG_AX)))
    print("  BX = 0x{:04X}".format(mu.reg_read(UC_X86_REG_BX)))
    print("  CX = 0x{:04X}".format(mu.reg_read(UC_X86_REG_CX)))
    print("  DX = 0x{:04X}".format(mu.reg_read(UC_X86_REG_DX)))
    print("  SI = 0x{:04X}".format(mu.reg_read(UC_X86_REG_SI)))
    print("  DI = 0x{:04X}".format(mu.reg_read(UC_X86_REG_DI)))
    print("  BP = 0x{:04X}".format(mu.reg_read(UC_X86_REG_BP)))
    print("  SP = 0x{:04X}".format(mu.reg_read(UC_X86_REG_SP)))


mu = Uc(UC_ARCH_X86, UC_MODE_16)

# In Unicorn, memory mapping must be aligned to the page size, which is typically 4KB. 
# If the address you provide is not aligned to a page boundary, Unicorn will raise an UC_ERR_ARG error.
page_size = 0x1000 # 4KB page size
page_start = ADDRESS & ~(page_size - 1) # round down to nearest multiple of page_size

mu.mem_map(page_start, 0x100000)

# boot loader
code = b"\xfa\xb8\xc0\x07\x8e\xd8\x8e\xc0\xb8\x00\x00\x8e\xd0\xbc\x00\x7c\xfb\xbe\x2b\x00\xe8\x01\x00\xc3\xbb\x00\x00\xac\x3c\x00\x74\x05\xe8\x03\x00\xeb\xf6\xc3\xb4\x0e\xcd\x10\xc3\x48\x65\x6c\x6c\x6f\x20\x57\x6f\x72\x6c\x64\x21\x00"
mu.mem_write(ADDRESS, code)

# print out assembly instructions
md = Cs(CS_ARCH_X86, CS_MODE_16)
for i in md.disasm(code, ADDRESS):
    print("0x%x:\t%s\t%s" %(i.address, i.mnemonic, i.op_str))
mu.hook_add(UC_HOOK_CODE, hook_code)

# run instructions
mu.emu_start(ADDRESS, ADDRESS + 0x17)

ORG 0x0	; bios will load this sector at 0x7c00 

BITS 16

_start:
	jmp short start
	nop

times 33 db 0 ; fake bios parameter block


start:
	jmp 0x7c0:step2

step2:
	cli	; clear interrupt
	
	mov	ax, 0x7c0
	mov	ds, ax		; this sets segment memory base
	mov	es, ax
	
	mov	ax, 0x0
	mov	ss, ax
	mov	sp, 0x7c00	; loads stack in front of our start

	sti	; enable interrupts

	mov	si, message
	call	print
	jmp	$

print:
	mov	bx, 0x0
.loop:
	lodsb
	cmp	al, 0x0
	je	.done
	call	print_char
	jmp	.loop	
.done:
	ret

print_char:
	mov	ah, 0xe
	int	0x10
	ret

message:
db 'Hello Lindsey!', 0



times 510 - ($ - $$) db 0 	; $ is current location
				; $$ is start
dw 0xAA55

